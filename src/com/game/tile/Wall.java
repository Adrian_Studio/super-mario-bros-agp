package com.game.tile;

import java.awt.Color;
import java.awt.Graphics;

import com.game.Game;
import com.game.Handler;
import com.game.Id;

public class Wall extends Tile{

	public Wall(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
		
	}

	public void render(Graphics g) {
		g.drawImage(Game.ground.getBufferedImage(), x, y, width, height, null);
	}

	public void tick() {

		
	}

}
