package com.game.tile;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.game.Handler;
import com.game.Id;

public abstract class Tile {

	public int x, y;
	public int width, height;
	
	public boolean solid;
	
	public int velX, velY;
	
	public Id id;
	
	public Handler handler;
	
	public Tile(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.solid = solid;
		this.id = id;
	}
	
	public abstract void render(Graphics g);
	public abstract void tick();

	public void die() {
		handler.removeTile(this);
	}
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public Id getId() {
		return id;
	}
	

	public boolean isSolid() {
		return solid;
	}

	public void setVelX(int velX) {
		this.velX = velX;
	}


	public void setVelY(int velY) {
		this.velY = velY;
	}
	
	public Rectangle getBounds() {
		//It makes the entity 1 pixel taller so you can compare when there is no collision but you are standing with it
		//It makes the entity 2 pixel wide so you can compare when there is no collision but you are sliding with it
		return new Rectangle(getX() - 1, getY() - 1, width + 2, height + 1); 
	}
	
}
