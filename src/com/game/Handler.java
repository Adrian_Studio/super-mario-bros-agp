package com.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import com.game.entity.Entity;
import com.game.entity.Player;
import com.game.tile.Tile;
import com.game.tile.Wall;

public class Handler {

	public LinkedList<Entity> entity = new LinkedList<Entity>();
	public LinkedList<Tile> tile = new LinkedList<Tile>();
		
	public void render(Graphics g) {
		for(Entity en: entity) {
			en.render(g);
		}
		for(Tile ti: tile) {
			ti.render(g);
		}
	}
	
	public void tick() {
		for(Entity en: entity) {
			en.tick();
		}
		for(Tile ti: tile) {
			ti.tick();
		}
	}
	
	public void addEntity(Entity en) {
		entity.add(en);
	}
	
	public void removeEntity(Entity en) {
		entity.remove(en);
	}
	
	public void addTile(Tile ti) {
		tile.add(ti);
	}
	
	public void removeTile(Tile ti) {
		tile.remove(ti);
	}
	
	
	public void createLevel(BufferedImage level) {
		int width = level.getWidth();
		int height = level.getHeight();
		
		for(int y=0; y < height; y++) {
			for(int x=0; x < width; x++) {
				int pixel = level.getRGB(x, y);
				
				int red = (pixel >> 16) & 0xff;
				int green = (pixel >> 8) & 0xff;
				int blue = (pixel) & 0xff;
				
				if(red == 0 && green == 0 && blue == 0) addTile(new Wall(x*32, y*32, 32, 32, true, Id.wall, this));
				if(red == 0 && green == 0 && blue == 255) addEntity(new Player(x*32, y*32, 32, 32, true, Id.player, this));
				//System.out.print(red +"," +green +"," +blue +" - ");
			}
			//System.out.println();
		}
		
	}
}
