package com.game.Input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.game.Game;
import com.game.entity.Entity;

public class KeyInput implements KeyListener {
	protected boolean pressedJump = false;
	protected boolean pressedRight = false;
	protected boolean pressedLeft = false;

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		for(Entity en:Game.handler.entity) {
			switch(key) {
			case KeyEvent.VK_SPACE:
			//case KeyEvent.VK_UP:
				if(!pressedJump) {
					if(en.getyState() != en.RISING && en.getyState() != en.FALLING) { //At this moment this let you push when you are falling creating problems (WE NEED TO FIX THAT)
						en.setJump(true);
					}else if((en.getyState() == en.RISING || en.getyState() == en.FALLING) && en.isSlidingVertically() && ((en.isLeftCollision() && pressedLeft) || (en.isRightCollision() && pressedRight))) {
						en.setDoubleJump(true);
					}
				}
				pressedJump = true;
				break;
//			case KeyEvent.VK_DOWN:
//				en.setVelY(5);
//				break;
			case KeyEvent.VK_LEFT:
				en.setxState(en.MOVING_LEFT);
				pressedLeft = true;
				break;
			case KeyEvent.VK_RIGHT:
				en.setxState(en.MOVING_RIGHT);
				pressedRight = true;
				break;
			}
		}
		
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		for(Entity en:Game.handler.entity) {
			switch(key) {
			case KeyEvent.VK_SPACE:
//			case KeyEvent.VK_UP:
				if(!en.isDoubleJump()) en.setyState(en.FALLING);
				pressedJump = false;
				break;
//			case KeyEvent.VK_DOWN:
//				en.setVelY(0);
//				break;
			case KeyEvent.VK_LEFT:
				if(en.getxState() == en.MOVING_LEFT) en.setxState(en.STOPPED);
				pressedLeft = false;
				break;
			case KeyEvent.VK_RIGHT:
				if(en.getxState() == en.MOVING_RIGHT) en.setxState(en.STOPPED);
				pressedRight = false;
				break;
			}
		}
		
	}


	public void keyTyped(KeyEvent e) {
		//Not used
	}
}
