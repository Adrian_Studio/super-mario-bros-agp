package com.game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import com.game.Input.KeyInput;
import com.game.entity.Entity;
import com.game.entity.Player;
import com.game.gfx.Sprite;
import com.game.gfx.SpriteSheet;
import com.game.tile.Wall;

public class Game extends Canvas implements Runnable{
	public static final int WIDTH = 640;
	public static final int HEIGHT = WIDTH/16 * 9;
	//SCALE = 2 for 1280x720 resolution
	//SCALE = 3 for 1920x1080 resolution
	public static final int SCALE = 2;
	
	/*Dimensiones originales Super Mario
	public static final int WIDTH = 256;
	public static final int HEIGHT = WIDTH/8 * 7;
	public static final int SCALE = 4;*/
	
	public static final String TITLE = "Super Mario Bros AGP";
	
	private Thread thread;
	private boolean running = false;
	private BufferedImage image;
	
	public static Handler handler;
	public static SpriteSheet sheet_ground;
	public static SpriteSheet sheet_mario_small;
	
	public static Sprite ground;
	public static Sprite[][] player;
	
	public static Camera cam;
	
	public Game() {
		Dimension size = new Dimension(WIDTH*SCALE, HEIGHT*SCALE);
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
	}
	
	private void init() {
		handler = new Handler();
		sheet_ground = new SpriteSheet("/img/ground/ground1.png");
		sheet_mario_small = new SpriteSheet("/img/players/Mario/mario_small.png");
		cam = new Camera();

		addKeyListener(new KeyInput());
		
		ground = new Sprite(sheet_ground, 1, 1);
		//player = new Sprite(sheet_mario_small, 1, 1);
		player = new Sprite[15][2];
		
		for(int i=0; i < player.length; i++) {
			player[i][0] = new Sprite(sheet_mario_small, i + 1, 1);
			player[i][1] = new Sprite(sheet_mario_small, i + 1, 2);
		}
		
		try {
			image = ImageIO.read(getClass().getResource("/img/levels/level_0.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		handler.createLevel(image);
		//If we can create an object without the external file (example with player)
		//handler.addEntity(new Player(300, 512, 32, 32, true, Id.player, handler));
	}
	
	private synchronized void start() {
		if(running) return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}	
	
	private synchronized void stop() {
		if(!running) return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		init();
		requestFocus();
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		double delta = 0.0;
		double ns = 1000000000.0/60.0;
		int frames = 0;
		int ticks = 0;
		while(running) {
			long now = System.nanoTime();
			delta += (now - lastTime)/ns;
			lastTime = now;
			while(delta >= 1) {
				tick();
				ticks++;
				delta--;
			}
			render();
			frames++;
			if(System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				//System.out.println(frames + " Frames Per Second " + ticks + " Updates Per Second");
				frames = 0;
				ticks = 0;
			}
		}
		stop();
	}
	
	public void render() {
		BufferStrategy bs  = getBufferStrategy();
		if(bs == null) {
			createBufferStrategy(3);
			return;
		}
		Graphics g =bs.getDrawGraphics();
		//If you want a defined color
		g.setColor(Color.BLACK);
		g.fillRect(0,0, getWidth(), getHeight());
		//If you want an undefined color
		//g.setColor(new Color(255,0,0));
		//g.fillRect(200, 200, getWidth()-400, getHeight()-400);
		g.translate(cam.getX(), cam.getY());
		handler.render(g);
		g.dispose();
		bs.show();
	}
	
	public void tick() {
		handler.tick();
		
		for(Entity e:handler.entity) {
			if(e.getId() == Id.player) {
				cam.tick(e);
			}
		}
	}
	
	public int getFrameWidth() {
		return WIDTH * SCALE;
	}
	
	public int getFrameHeight() {
		return HEIGHT * SCALE;
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		JFrame frame = new JFrame(TITLE);
		frame.add(game);
		frame.pack();
		frame.setResizable(false);
		//For some reason frame.setLocationRelativeTo(null); don't set the window in the exact center.
		frame.setLocationRelativeTo(null);
		//For some reason frame.setLocation(0,0) don't set the window on the top-left corner of the screen.
		//frame.setLocation(-8,0);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		game.start();
	}

}
