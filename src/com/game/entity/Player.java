package com.game.entity;

import java.awt.Color;
import java.awt.Graphics;

import com.game.Game;
import com.game.Handler;
import com.game.Id;
import com.game.tile.Tile;

public class Player extends Entity{
	
	
	
	public Player(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);

	}

	public void render(Graphics g) {
		int i = 0;
		int j = 0;
		if (yState == NOT_JUMPING && xState == STOPPED) {
			i = 0;
		}if ((yState == NOT_JUMPING && xState != STOPPED && !slidingHorizontally) || (yState == NOT_JUMPING && xState == STOPPED && velX != 0 && !slidingHorizontally)) {
			if(lastMovingAnimation >= 6 && lastMovingAnimation <= 8) {
				if(velX <= 5) {
					lastMovingAnimation += 0.01; //We set the animation minimum velocity so we can see it better because is a sequence of frames
				}else {
					lastMovingAnimation += 0.002 * Math.abs(velX); //We do the animation related to the X axis velocity so we can appreciate the velocity because is a sequence of frames.
				}
				
				i = (int) lastMovingAnimation;
			}else {
				lastMovingAnimation = 6;
				i = (int) lastMovingAnimation;
			}
		}else if(yState == RISING || yState == FALLING) {
			i = 1;
		}else if(slidingHorizontally) {
			i = 5;
		}
		
		if (xState == MOVING_RIGHT || (xState == STOPPED && lastXMovement == MOVING_RIGHT)) {
			j = 0;
		}else if(xState == MOVING_LEFT || (xState == STOPPED && lastXMovement == MOVING_LEFT)) {
			j = 1;
		}
		
		
		g.drawImage(Game.player[i][j].getBufferedImage(), x, y, width, height, null);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		standing = false;
		slidingVertically = false;
		rightCollision = false;
		leftCollision = false;
		//The comparison values are the window resolution
		/*if(x <= 0) {
			x = 1;
			velX = 0;
			auxVelX = 0;
		}
		if(x + width >= 1280) {
			x = 1280 - width -1;
			velX = 0;
			auxVelX = 0;
		}
		if(y + height >= 720) y = 720 - height;*/
		for(Tile t:handler.tile) {
			if(!t.solid) break;
			if(t.getId() == Id.wall) {
				if(getBoundsTop().intersects(t.getBounds())) {
					setVelY(0);
					if(yState == RISING) {
						yState = FALLING;
						auxVelY = 0.0F;
					}
					y = t.getY() + t.height;
				}
				if(getBoundsLeft().intersects(t.getBounds())) {
					setVelX(0);
					x = t.getX() + t.width;
					if((yState != NOT_JUMPING && xState == MOVING_LEFT) || (yState == NOT_JUMPING && (slidingHorizontally || xState == MOVING_LEFT))) {
						velX = 0;
						auxVelX = 0;
					}
					if(yState == RISING || yState == FALLING) slidingVertically = true;
					leftCollision = true;
				}
				if(getBoundsRight().intersects(t.getBounds())) {
					setVelX(0);
					x = t.getX() - width;
					if((yState != NOT_JUMPING && xState == MOVING_RIGHT)  || (yState == NOT_JUMPING && (slidingHorizontally || xState == MOVING_RIGHT))) {
						velX = 0;
						auxVelX = 0;
					}
					if(yState == RISING || yState == FALLING) slidingVertically = true;
					rightCollision = true;
				}
				if(getBoundsBottom().intersects(t.getBounds())) {
					setVelY(0);
					if(yState == FALLING) {
						yState = NOT_JUMPING;
						slidingVertically = false;
					}
					y = t.getY() - height;
					velY = 0;
					auxVelY = 0;
					standing = true;
				}
			}
			
		}
		
		if(jump) {
			yState = RISING;
			auxVelY = jumpVelY;
			jump = false;
		}
		/*if(doubleJump) {
		
		}*/
		if(yState == NOT_JUMPING && (!standing || slidingVertically)) yState = FALLING;
		if(yState == RISING) {
			auxVelY -= gravity;
			setVelY((int) -auxVelY);
			if(auxVelY <= 0.0) {
				yState = FALLING;
			}
		}
		if(yState == FALLING) {
			auxVelY += gravity;
			if(auxVelY > maxVelY) auxVelY = maxVelY;
			setVelY((int)auxVelY);
		}
		
		if(yState == NOT_JUMPING) {
			auxVelY = 0;
			setVelY((int) auxVelY);
		}
		
		if(xState == STOPPED || (xState == MOVING_RIGHT && rightCollision) || (xState == MOVING_LEFT && leftCollision)) {
			if((lastXMovement == MOVING_RIGHT && auxVelX > 0 && !slidingHorizontally) || (lastXMovement == MOVING_LEFT && auxVelX > 0 && slidingHorizontally)) {
				auxVelX -= accelX;
				setVelX((int) auxVelX);
			}else if((lastXMovement == MOVING_LEFT && auxVelX < 0 && !slidingHorizontally) || (lastXMovement == MOVING_RIGHT && auxVelX < 0 && slidingHorizontally)) {
				auxVelX += accelX;
				setVelX((int) auxVelX);
			}else{
				auxVelX = 0;
				setVelX(0);
				slidingHorizontally = false;
			}
			
		}
		if(xState == MOVING_RIGHT && (!slidingVertically || leftCollision)) {
			lastXMovement = MOVING_RIGHT;
			if (velX < 0) {
				slidingHorizontally = true;
				auxVelX += accelX * 3;
				velX = (int) auxVelX;
			}else if (velX < maxVelX) {
				slidingHorizontally = false;
				auxVelX += accelX;
				velX = (int) auxVelX;
			}else {
				slidingHorizontally = false;
				auxVelX = maxVelX;
				velX = maxVelX;
			}
			
			
		}
		if(xState == MOVING_LEFT && (!slidingVertically || rightCollision)) {
			lastXMovement = MOVING_LEFT;
			if (velX > 0) {
				slidingHorizontally = true;
				auxVelX -= accelX * 3;
				velX = (int) auxVelX;
			}else if (velX > -maxVelX) {
				slidingHorizontally = false;
				auxVelX -= accelX;
				velX = (int) auxVelX;
			}else {
				slidingHorizontally = false;
				auxVelX = -maxVelX;
				velX = -maxVelX;
			}
			
		}
		//System.out.println(leftCollision + " - " + rightCollision);
	}

}
