package com.game.entity;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.game.Handler;
import com.game.Id;

public abstract class Entity {

	//Position of the entity
	public int x, y;
	
	//Height and width of the entity
	public int height, width;
	
	//Indicates if the entity is solid or an delusion
	public boolean solid;
	
	// State of the entity respect the X axis.
	public static final int STOPPED = 0;
	public static final int MOVING_RIGHT = 1;
	public static final int MOVING_LEFT = -1;
	
	// State of the entity respect the Y axis.
	public static final int NOT_JUMPING = 0;
	public static final int RISING = -1;
	public static final int FALLING = 1;

	/* Indicate the actual state respect the X axis.
	 * Possible values:
	 * STOPPED, MOVING_RIGHT, or MOVING_LEFT. */
	protected int xState;
	
	//Saves the last movement in the X axis to determine the looking direction of the player
	protected int lastXMovement = MOVING_RIGHT;
	
	//Saves the last animation of the moving entity so you can follow a pattern
	protected float lastMovingAnimation = 0;
	
	//Auxiliary to do something when its standing in an object
	protected boolean standing = false;
	
	//Auxiliary to do something when its sliding vertically in an object
	protected boolean slidingVertically = false;
	
	//Auxiliary to do something when its sliding horizontally in an object
	protected boolean slidingHorizontally = false;
	
	//Auxiliary to jump if its standing in an object
	protected boolean jump = false;
	
	//Auxiliary to jump again if its sliding vertically in an object
	protected boolean doubleJump = false;
	
	//Auxiliary to know which side of the player is in contact with an object.
	protected boolean rightCollision = false;
	protected boolean leftCollision = false;
	
	/* Indicate the actual state respect the Y axis.
	 * Possible values:
	 * NOT_JUMPING, RISING, or FALLING. */
	protected int yState = FALLING;

	//Actual velocity in each axis
	protected int velX, velY;
	
	//Auxiliary of the actual velocity in each axis
	protected float auxVelX = 0.0F;
	protected float auxVelY = 0.0F;
	
	protected Id id;
	//Gravity acceleration and deceleration
	protected float gravity = 0.2F; //accelY
	
	//Gravity acceleration and deceleration
	protected float accelX = 0.1F;
	
	public int maxVelX = 3;
	public int maxVelY = 10;
	public int jumpVelY = 10;
	public int jumpVelX = 4;
	
	public Handler handler;
	
	public Entity(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.solid = solid;
		this.id = id;
		this.handler = handler;
	}
	
	public abstract void render(Graphics g);
	public abstract void tick();

	public void die() {
		handler.removeEntity(this);
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public Id getId() {
		return id;
	}

	public boolean isSolid() {
		return solid;
	}

	public void setVelX(int velX) {
		this.velX = velX;
	}


	public void setVelY(int velY) {
		this.velY = velY;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(getX(), getY(), width, height); 
	}
	
	public Rectangle getBoundsTop() {
		return new Rectangle(getX() + 10, getY(), width - 20, 1);
	}
	
	public Rectangle getBoundsBottom() {
		return new Rectangle(getX() + 1, getY() + height - 1, width - 2, 1);
	}
	
	public Rectangle getBoundsLeft() {
		return new Rectangle(getX(), getY() + 10, 1, height - 20);
	}
	
	public Rectangle getBoundsRight() {
		return new Rectangle(getX() + width - 1, getY() + 10, 1, height - 20);
	}

	public int getxState() {
		return xState;
	}

	public void setxState(int xState) {
		this.xState = xState;
	}

	public int getyState() {
		return yState;
	}

	public void setyState(int yState) {
		this.yState = yState;
	}

	public boolean isSlidingVertically() {
		return slidingVertically;
	}

	public void setSlidingVertically(boolean slidingVertically) {
		this.slidingVertically = slidingVertically;
	}

	public boolean isDoubleJump() {
		return doubleJump;
	}

	public void setDoubleJump(boolean doubleJump) {
		this.doubleJump = doubleJump;
	}

	public boolean isJump() {
		return jump;
	}

	public void setJump(boolean jump) {
		this.jump = jump;
	}

	public boolean isRightCollision() {
		return rightCollision;
	}

	public void setRightCollision(boolean rightCollision) {
		this.rightCollision = rightCollision;
	}

	public boolean isLeftCollision() {
		return leftCollision;
	}

	public void setLeftCollision(boolean leftCollision) {
		this.leftCollision = leftCollision;
	}
	
}
